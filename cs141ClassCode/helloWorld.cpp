// Read the instructions from the file "iostream"
#include <iostream>

// Allow references to objects in the std namespace without using
// the full names of those objects.
using namespace std;

int main()
{
	// Write a string of characters to the output stream 
	// that is associated with the console.
	cout << "Hello bitbucket!" << endl;

	// Wait for the user to press return before ending the program.
	// This will allow us to read the program output.
	system("pause");
}